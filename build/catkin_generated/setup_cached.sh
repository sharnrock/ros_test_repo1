#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CATKIN_TEST_RESULTS_DIR="/home/vkeith/ros_test_ws/build/test_results"
export ROS_TEST_RESULTS_DIR="/home/vkeith/ros_test_ws/build/test_results"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/vkeith/ros_test_ws/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/vkeith/ros_test_ws/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/vkeith/ros_test_ws/devel/lib:/home/vkeith/ros_test_ws/devel/lib/i386-linux-gnu:/opt/ros/indigo/lib/i386-linux-gnu:/opt/ros/indigo/lib"
export PATH="/home/vkeith/ros_test_ws/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/vkeith/ros_test_ws/devel/lib/pkgconfig:/home/vkeith/ros_test_ws/devel/lib/i386-linux-gnu/pkgconfig:/opt/ros/indigo/lib/i386-linux-gnu/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PWD="/home/vkeith/ros_test_ws/build"
export PYTHONPATH="/home/vkeith/ros_test_ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/vkeith/ros_test_ws/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/vkeith/ros_test_ws/src:$ROS_PACKAGE_PATH"