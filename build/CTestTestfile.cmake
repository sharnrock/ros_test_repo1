# CMake generated Testfile for 
# Source directory: /home/vkeith/ros_test_ws/src
# Build directory: /home/vkeith/ros_test_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(mission_planner)
